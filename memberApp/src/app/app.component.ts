import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isData = false;
  searchVal : string;
  displayData(event){
    this.searchVal=event.target.value;
    this.isData = true;
  }

}
